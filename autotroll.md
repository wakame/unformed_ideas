# Autotroll

## Tags
Software, Automation, Assistant

## Idea
A software that makes appointments for you. Or requests information from other people.
With scalable aggressiveness and automated mailing/calling.

## Problem description

Making appointments or getting some important piece of information from someone can be a time-consuming task.
Some people / companies tend to play the "ignore" game, hoping you will give up eventually.
Others are simply unorganized, saying things like "I will write it down, check it later, then call you back".
Which of course they never will.

## Potential solution

Autotroll is a software, maybe on a local device, maybe somewhere in the cloud, that automates interacting with
annoying people. In a passive-aggressive way that only a machine can exhibit.

You create Autotroll tasks (e.g. make a doctors appointment), optionally specify a deadline or other additional information,
and Autotroll will use provided phone numbers and/or email addresses to contact the doctor.
Over time, Autotroll will become more aggressive, instead of sending one mail per day, it will be one per hour.
Or it will call every five minutes while at the doctors office.

The passive-aggressiveness extends to the way information exchange is performed. Emails will e.g. contain a list of questions,
email answers that don't fit the required format (e.g. a date, a time, a phone number) will be ignored, if e.g. a date has
been found in the email, a later Autotroll email will ask for confirmation that this is the date of the appointment.

For phones, Autotroll will use text to speech to tell the person on the other end what to do, accepting voice answers,
number inputs or simple yes/no answers.

### But what if you are Autotrolled?

Of course, Autotroll should be able to detect other Autotroll instances. There could be an explicit handshake, allowing two
instances to directly exchange useful information.
The nature of Autotroll makes it highly desirable to buy your own license of Autotroll, if only to defend against other
Autotroll users. In the long run, this could result in an automated information exchange approach between people and companies.
