# Cheap Small Computer

## Tags
Electronics, Hardware, Experimenting, Teaching

## Idea

Have a simple, small computer for certain teaching/experimenting purposes.
Should be very cheap and self-contained (requiring no external hardware).

## Problem description

We have computer/hardware teaching materials for children and/or enthusiasts.
Mostly stuff like Raspberry Pi, Arduino, Micro:bit or Callisto.

Some positive aspects:
* Often comes with documentation/tutorials/experiments.
* At least Micro:bit and Callisto have some sensors and actuators, so at least "some batteries included".

Drawbacks of these devices:
* Needs a monitor (Raspberry Pi) or even a whole computer for programming the device.
* Has custom PCBs, SMD elements.
* "Relatively" expensive. Especially if you need to buy a monitor or even a PC for it.

Therefore: Why not build a completely stand-alone computer? That is also cheap? And can be assembled/repaired by the user?

## Potential solution

Use a cheap small microcontroller like e.g. an ATtiny, a low-price mini display and for input
dip switches to toggle in memory values.

Two dip switches with 8 bit each allow adressing 64Kb, one dip switch is used to specify the value. A button writes the value.

Still requires a custom program flashed one time on the microcontroller that has yet to be written:
* Reading the dip switches and writing to memory.
* Jump to the memory to start the toggled-in program.
* Maybe support for a reset button?
* Maybe running an emulator with a nicer instruction set?

### Upgrade 1: Joystick and buttons for games/simple interaction

Joystick plus two or four push buttons places somewhere ergonomic.

### Upgrade 2: Punch-card reader

Punch cards allow "writing down" programs that are then fed into the computer.
Might be easier than toggling in programs (meaning: less error prone).

By building a reader using five photo diodes, a card or paper can be read, provided a light source is available.
Four diodes read four binary values, the fifth diode triggers the reading and storing of a value.
Can of course be extended to more values that are being read at the same time. Or less.

## Hardware editions

<pre>
TODO The actual wiring is missing, as well as a solution to the obvious problem to attach all the inputs to the ATtiny. Each Dip switch row needs their own 74HC165 (if 3 pins per row are okay). Alternatively PCF8574/PCF8574A as port expanders over I2C. Maybe a microcontroller with more pins, because it look ridiculous if the actual controller is smaller than the port expanders.
</pre>

### Handcrafted low-price edition

![Layout of the components](csc_images/mini_computer.svg)

<pre>
| Pos | Amount | Description  |  Price |
|   1 |      1 | ATtiny 13A-PU | 1.45 € |
|   2 |      1 | DIP socket (for ATtiny) | 0,04 € |
|   3 |      1 | 3v3 Boost converter | 0,99 € |
|   4 |      1 | Led Display 0,96 Zoll I2C 128x64 Pixel |  3,19 € | 
|   5 |      3 | Dip Switch (with 8 switches) | 1,08 € |
|   6 |      1 | Battery holder 1,5 Volt AAA | 0,36 € |
|   7 |      1 | Push button | 0,16 € |
|   8 |      1 | Toggle switch | 0,60 € |
|   9 |      1 | Stripboard + wires | 3€ |
</pre>

*Upgrade 1*
<pre>
|   x |      1 | Mini analog joystick | 2€ |
|   x |    2-4 | More "thumb friendly" push buttons | 1.70-3.40 € |
</pre>

*Upgrade 2*
<pre>
|   x |      5 | Photo diode | 0,60 € |
</pre>

<pre>
TODO Some prices guessed
TODO Not tried out yet
TODO Replace toggle switch with something cheaper and less bulky?
</pre>

### Arduino-based ("Less assembly required edition")

<pre>
| Pos | Amount | Description  |  Price |
|   1 |      1 | Arduino Nano | 10-20€ |
|   2 |      1 | Led Display 0,96 Zoll I2C 128x64 Pixel |  3,19 € | 
|   3 |      1 | USB Cable | 3€ |
|   4 |      1 | Power bank | 10€ |
|   5 |      3 | 8er Dip Switch | 1,08 € |
|   6 |      1 | Breadboard + Wires | 5 € |
</pre>

<pre>
TODO Some prices guessed
TODO Not tried out yet
</pre>
