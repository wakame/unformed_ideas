# Fediverse of trust

## Tags
Software, Social Network, Spam

## Idea
Automatically rate a post in a social network based on a posters trustworthiness relative to your part of the social network.
For every post you directly rated (by favoriting, boosting, muting, blocking or even a float value from -1.0 to 1.0) your rating of the poster is recalculated.
If a poster who you have never rated posts something, the rating can indirectly be calculated by calculating one or more shortests paths through the network to people who have rated the poster.

## Problem description

Social networks can easily be used to distribute hate speech, fake news or simply topics that a given user doesn't care about or doesn't like to read about.
Centralized approaches to filter content are based on spam filters, user flagging, a post review team etc.

In a decentralized social network, content filtering is done more on a per-instance basis. Less important here is the technology decentralization, but the question _who_ decides what is appropriate content.

Especially if you take into account more "soft" filtering, e.g. filtering content that is not "universally forbidden", but certain sensitive topics for example, things become complicated.

## Potential solution

The trivial solution: Have a rating value for every user on the network and filter the content you consume by adjusting the cutoff value.
A little bit more finegrained: Flag certain words as positive or negative to calculate a post rating that is both influenced by the posters rating and the content.

Obvious problem: There are a lot of posters, people can easily create new accounts, rating every user is therefore not practical.

Improved solution: Trust in the judgement of people that are "close" to you on the social network. You could either manually add them to a list or have them automatically selected based on your ratings of posters you communicate with.

### Practical example

Alice has set her cutoff value to 0 on her new account. She hasn't rated anything yet, so the content that makes it through her filters is basically "everything that is allowed according to the terms of the instance".

She reads some posts about donuts about a poster named "Bob". She favorites a few of his posts, giving them a positive rating. This results in the overall rating of Bob becoming a positive value (0.1 in this example).

Later, Carol posts a cat picture. Alice account has no information about her, so it queries Bobs account. Bobs rating of Carol is very positive (0.5), so multiplying Alices trust in Bob with Bobs trust in Carol results
in `0.1*0.5 = 0.05`, still a positive value.

After a while, Alice becomes a bit annoyed from most of the posts on the network, so she sets the cutoff to 0.1. Bobs remarks about the mental health of muffin lovers still make it through, but Carols other cat pictures don't.

Later, Dave, a professional muffin baker, engages in a flame war with Bob. Or at least tries to, since Bob has given a rating of -0.5 and doesn't even receive Daves messages.

Alice has since lowered her cutoff value to -0.15 because she wants to have a look at some of the stuff that she would normally filter out. Her rating of Bob is now 0.2, so Daves message is rated -0.1 and still makes it over the cutoff.

She is obviously enraged about some person writing about muffins like they tasted better than cardboard, so she rates the post accordingly as -1.0. This puts the post below her threshold and even influcences Daves rating (now -.2) enough
to block all of Daves future posts, at least given her current cutoff value.
