# Rapid Electronics

## Tags
Electronics, Software, Tool

## Idea

A tool to build (simple) electronic circuits for breadboard or PCB.
Aimed to understand the circuit and help the occasion dabbler in electronics by preventing simple mistakes.

## Problem description

Imagine building a simple device: An Arduino Nano, a temperature sensor, a display.
Or maybe an ATtiny 45 instead? Or that other display you found in your pile of stuff?
How about a battery? Now, will _this_ battery work with _that_ microcontroller? Am I exceeding some voltage ratings?
Do I need a boost converter or something like that?

## Potential solution

Now imagine a graphical tool where you can throw in the components you want to connect.
A microcontroller, a sensor, a battery.

The tool will try to make sense out of that:
* Components need power: The battery is a power supply.
* A sensor is in an by itself pretty useless, so it will likely be connected to something else: The microcontroller.

Then, the tool will check the details:
* 1v5 AA battery: Not enough for the 5v stable or 7-12v not-so-stable voltage required for the Arduino Nano.
* The IR sensor is compatible to the ratings of the pin.

The tool might then add wires and components required so that the whole setup makes sense. Maybe in a "tool color"/"tool look" so that the user knows that is was added by the tool.

The user could also manually add more-or-less explicit connections, for example:
* A simple arrow that states: "I want to connect this thing with this pin, but I am open to suggestions."
* A wire stating: "I want these two connected, no matter what."

